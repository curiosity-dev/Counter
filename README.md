# Counter
A nice way to count in Discord.

## How to setup Counter

**Prerequisites**:
- [ ] Git
- [ ] Node.js (I used version 8.9.3 to develop this script and it worked for me)
- [ ] Npm (Usually comes installed with Node.js)

**Steps**:
- If you haven't created a Discord webhook yet, go to your Server Settings -> Webhooks -> Create Webhook. Give it a nice name and icon (optional) and pick the channel it will send messages to. Then, copy the URL by clicking the button.
- Setup your environment variables. There are 2 methods:
  **Method 1**:
    - Add the following environment variables to your system by running this:
      ```
      $ set WEBHOOK_URL=discord webhook url you copied earlier
      $ set COUNT_TO=number to count to
      ```
    - Done.
  **Method 2**:
    - Run the following command to copy the .env.example file to a regular .env file
      ```
      $ copy .env.example .env
      ```
    - Open the .env file and edit the `discord webhook url here` and `number to count to here` texts with your data.
    - Save the file and continue.
- Next, in your terminal, run the following command to install dependencies and run the script:
  ```
  $ npm run quickrun
  ```
  This will install everything it needs to run and then will run the script for you.
- As soon as the script loads, it should start counting in the channel you setup the webhook for. Enjoy!

If there are any errors or issues, [click here](https://github.com/CuriosityDev/Counter/issues/new) to create a new issue on GitHub.

## License
This project is licensed with the **MIT License**. You can find the license in the LICENSE.txt file in this repository.
