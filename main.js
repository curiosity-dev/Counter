// Import some libraries
const Discord = require('discord.js'); // Discord API wrapper for Node.js

// Verify that all the environment variables are there
["WEBHOOK_URL", "COUNT_TO"].forEach(env => {
  if (!process.env.hasOwnProperty(env)) {
    // If the environment variable is missing, use dotenv to see if it exists in the .env file
    console.log(`Missing environment variable ${env}, reading from .env file...`);
    require('dotenv').config(); // Use dotenv
  }
});

var webhook_url = process.env.WEBHOOK_URL; // Get the URL of the webhook to count on.
var webhook_id = webhook_url.split("/")[5]; // Get the ID of the webhook
var webhook_token = webhook_url.split("/")[6]; // Get the token of the webhook
const webhook = new Discord.WebhookClient(webhook_id, webhook_token); // Create a new WebhookClient to make stuff easier

var count_to = parseInt(process.env.COUNT_TO); // Get the Number version of the count_to string
var current = 0; // The current number we're at, 0.

// Simple setInterval() to count +1 every 3 seconds (3000 milliseconds)
var counting = setInterval(function() {
  if (current <= count_to) {
    // If the current number is less than the count_to, then count
    console.log(`Counting... ${current}`);
    try {
      webhook.send(current.toString());
    } catch (CountError) {
      console.error(`Counting Error: ${CountError}`);
    }
    current += 1;
  } else {
    // If it's not less than the count_to (aka it's greater) then stop counting and let the user know.
    console.log(`Counted successfully to ${count_to}`);
    clearInterval(counting);
    process.exit(0);
  }
}, 3000);

// Handle promise errors in case we get one
process.on("unhandledRejection", promiseErr => {console.error(`[Unhandled Promise Error] ${promiseErr}`)});
